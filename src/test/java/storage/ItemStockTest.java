package storage;
import org.junit.*;
import cz.cvut.eshop.storage.*;
import cz.cvut.eshop.shop.*;
/** @author Vladimir Dvorak
 */
public class ItemStockTest {
    Item item1;
    ItemStock itemStock;    
    
    @Before
    public void init(){
        item1 = new StandardItem(1, "computer", 100.45f, "computer", 10);
        itemStock = new ItemStock(item1);
    }
}