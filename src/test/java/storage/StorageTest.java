package storage;
import org.junit.*;
import cz.cvut.eshop.archive.*;
import cz.cvut.eshop.shop.*;
import cz.cvut.eshop.storage.*;
import java.util.*;

/** 
 * @author Vladimir Dvorak
 */
public class StorageTest {    
    Storage storage;
    
    @Before
    public void init(){
        HashMap<Integer, ItemStock> stock = new HashMap<Integer, ItemStock>();
        storage = new Storage(stock);
    }




















    @Test
    public void insertItems_getCorrectItemsInStock(){
        Item item1 = new StandardItem(1, "computer", 100.45f, "computer", 10);
        Item item2 = new StandardItem(2, "mobile", 60.70f, "mobile", 9);
        storage.insertItems(item1, 1);
        storage.insertItems(item2, 2);
        Assert.assertEquals(2, storage.getStockEntries().size());
    }

    @Test
    public void removeItems_removeCorrectAllItems() throws NoItemInStorage{
        Item item1 = new StandardItem(1, "computer", 100.45f, "computer", 10);
        storage.insertItems(item1, 1);
        storage.removeItems(item1, 1);
        Assert.assertEquals(0, storage.getStockEntries().size());
    }
    
    @Test
    public void getItemCount_getCorrectItemCount(){
        Item item1 = new StandardItem(1, "computer", 100.45f, "computer", 10);
        storage.insertItems(item1, 8);
        Assert.assertEquals(8, storage.getItemCount(item1));
    }

    @Test
    public void getItemCount_getCorrectItemCountWithIntegerInput(){
        Item item1 = new StandardItem(1, "computer", 100.45f, "computer", 10);
        storage.insertItems(item1, 9);
        Assert.assertEquals(9, storage.getItemCount(item1.getID()));
    }

    @Test
   public void getPriceOfWholeStock_getCorrectPriceOfStock(){
        Item item1 = new StandardItem(1, "computer", 200, "computer", 10);
        Item item2 = new StandardItem(2, "computer", 110, "computer", 11);
        Item item4 = new StandardItem(4, "mobile", 70, "mobile", 9);
        Item item5 = new StandardItem(5, "mobile", 80, "mobile", 8);
        storage.insertItems(item1, 1);
        storage.insertItems(item2, 1);
        storage.insertItems(item4, 1);
        storage.insertItems(item5, 1);
        Assert.assertEquals(460, storage.getPriceOfWholeStock());
    }

    @Test
    public void getItemsOfCathegorySortedByPrice_getCorrectItemsInCathegory(){
        boolean isCorrect = true;        
        Item item1 = new StandardItem(1, "computer 1", 400, "computer", 10);
        Item item2 = new StandardItem(2, "computer 2", 110, "computer", 11);
        Item item4 = new StandardItem(4, "computer 4", 70, "computer", 16);
        Item item5 = new StandardItem(5, "computer 5", 80, "computer", 17);
        Item item6 = new StandardItem(6, "mobile 1", 180, "mobile", 9);
        Item item7 = new StandardItem(7, "mobile 2", 190, "mobile", 8);
        
        storage.insertItems(item1, 1);
        storage.insertItems(item2, 1);
        storage.insertItems(item4, 1);
        storage.insertItems(item5, 1);
        storage.insertItems(item6, 1);
        storage.insertItems(item7, 1);
        
        Collection<Item> collection = storage.getItemsOfCategorySortedByPrice("computer");
        
        Item itemList[] = new Item[4];
        collection.toArray(itemList);
        
        for(int i = 0; i < itemList.length - 1; i++){
            if(itemList[i].getPrice() > itemList[i + 1].getPrice()){
                isCorrect = false;
            }
        }
        
        Assert.assertEquals(true, isCorrect);
        
    }
























@Test(expected = java.lang.NullPointerException.class)
public void insertItems_putNullItemToStock(){
    Item item1 = null;
    storage.insertItems(item1, 1);
    Assert.assertEquals(0, storage.getStockEntries().size());
}

@Test
public void insertItems_putZeroCountOfItem(){
    Item item1 = new StandardItem(1, "computer", 100.45f, "computer", 10);
    storage.insertItems(item1, 0);
    Assert.assertEquals(1, storage.getStockEntries().size());
}


@Test(expected = java.lang.NullPointerException.class)
public void proccesOrder_testWithNullListOfItems() throws NoItemInStorage{
    ArrayList<Item> itemList = null;
    ShoppingCart shoppingCart = new ShoppingCart(itemList);
    Order order = new Order(shoppingCart);
    storage.processOrder(order);
    Assert.assertEquals(0, this.storage.getStockEntries().size());
}


@Test(expected = java.lang.NullPointerException.class)
public void getItemCount_findNullItemInStock(){
    Item item1 = null;
    int count = storage.getItemCount(item1);
    Assert.assertEquals(0, count);
}

@Test
public void getItemsOfCathegorySortedByPrice_putNullCathegoryToSort(){
    Item item1 = new StandardItem(1, "computer", 100.45f, "computer", 10);
    this.storage.insertItems(item1, 1);
    Collection<Item> collectionItem = this.storage.getItemsOfCategorySortedByPrice(null);
    Assert.assertEquals(0, collectionItem.size());
}

@Test(expected = NoItemInStorage.class)
public void removeItems_removeMoreItemsFromStock() throws NoItemInStorage{
    Item item1 = new StandardItem(1, "computer", 100.45f, "computer", 10);
    this.storage.insertItems(item1, 1);
    this.storage.removeItems(item1, 2);
    Assert.assertEquals(1, this.storage.getStockEntries().size());
}

public void endTest(){
    this.storage = null;
    int i = 0;
}

}